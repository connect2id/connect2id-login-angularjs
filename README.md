# Connect2id Server Login / Logout UI

A [Connect2id server](https://connect2id.com/products/server) login page
implemented with sleek AngularJS and Spring. Ready for deployment in
production.

Features:

 - Simple authentication and consent forms for OpenID Connect.
 - Authenticates users against an LDAP directory via the Connect2id [LdapAuth
   JSON-RPC 2.0 service](https://connect2id.com/products/ldapauth). An
   interface is provided to replace LDAP authentication with other methods, or
   add additional authentication factors.
 - Integrated via the [authorization session
   API](https://connect2id.com/products/server/docs/integration/authz-session)
   of the Connect2id server to process received [OpenID Connect authentication
   requests](https://connect2id.com/products/server/docs/api/authorization#auth-request),
   present the required forms, and return the encoded [authentication
   response](https://connect2id.com/products/server/docs/api/authorization#auth-response)
   to the client application (relying party).
 - Implements an optional [logout
   endpoint](https://connect2id.com/products/server/docs/api/logout) integrated
   via the [logout session
   API](https://connect2id.com/products/server/docs/integration/logout-session)
   of the Connect2id server.
 - Utilises of the [session store
   API](https://connect2id.com/products/server/docs/integration/session-store)
   of the Connect2id server to display information about the user and also to
   facilitate logout that isn't initiated by a client (relying party).


Compatible with **Connect2id Server 6.8+**.

## Quick Start

1. Clone this repository: `git clone https://bitbucket.org/connect2id/connect2id-login-angularjs c2id-login`
2. Install NodeJS, [Bower](https://bower.io) and [Gulp](https://www.npmjs.com/package/gulp), then run `cd c2id-login && npm install && bower install && gulp build`
3. Build the project with `./mvnw clean package`
4. Run the Login Page with:
```
java -jar -Dc2id.endpoint=https://demo.c2id.com \
	-Dc2id.ldapAuth.endpoint=https://demo.c2id.com/ldapauth/ target/c2id-login-*.war
```
5. Open the [OpenID Connect Client](https://demo.c2id.com/oidc-client) and initiate a new request:
    1. First, edit the "Authorization endpoint" field and enter "http://localhost:8000"
    2. Open the last tab "Authenticate end-user" and click "Log in with OpenID Connect"

If all goes well, a popup should appear with a login prompt, where you can enter a test username "alice" and the test
password "secret".

## OpenID Connect Endpoints

This package provides a web UI for these OpenID Connect Provider endpoints of a
Connect2id server:

 - The [authorization endpoint](https://connect2id.com/products/server/docs/api/authorization)
   where users authenticate and give their consent to log into the requesting
   client application and optionally grant an access token to it. This endpoint
   is mandatory for an OpenID Connect Provider. Its URL
   (`https://[login-page-base-url]/`) must be set in the
   [op.authz.endpoint](https://connect2id.com/products/server/docs/config/core#op-authz-endpoint)
   configuration property.

 - [Logout endpoint](https://connect2id.com/products/server/docs/api/logout)
   where users may be sent after they log out of a client application, to also
   log out of the Connect2id server. This endpoint is optional for an OpenID
   Connect provider. Its URL (`https://[login-page-base-url]/logout`) must set
   in the [op.logout.endpoint](https://connect2id.com/products/server/docs/config/core#op-logout-endpoint)
   configuration property.


## Configuration

Three YAML configuration files located in `src/main/resources/config` are
provided:

 - `application.yml` - default configuration
 - `application-dev.yml` - for development
 - `application-prod.yml` - for production

Edit these **before** building and packaging the WAR file with `mvn package` (see below).

Example configuration with some default values:

```yaml
c2id:
    # The base Connect2id server URL
    endpoint: http://127.0.0.1:8080/c2id

    # Sets the name of the session cookie
    cookieName: sid

    # The context path where this app is deployed (if it is outside the root directory).
    # This is read by Gulp and gets injected in the final index.html as <base href="${contextPath}">.
    # It is essential that this is pointing to the correct path, otherwise the routing in AngularJS will break!
    contextPath: /c2id-login/

    # The user authenticator to call
    authenticatorClass: LdapUserAuthenticator

    # Connect2id authorisation session API vars
    # See https://connect2id.com/products/server/docs/integration/authz-session
    authz:
        resourcePath: /authz-sessions/rest/v3
        accessToken: ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6
        # Set to `true` to cause the user's consent to be remembered for future
        # authorization requests, `false` for one-time authorization
        longLived: true

    # Connect2id user session store API vars
    # See https://connect2id.com/products/server/docs/integration/session-store
    session:
        resourcePath: /session-store/rest/v2
        accessToken: ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6

    # LdapAuth API var (username + password verification against an LDAP directory)
    # See https://connect2id.com/products/ldapauth/web-api
    ldapAuth:
        endpoint: http://localhost:8080/ldapauth/
        apiKey: f70defbeb88141f88138bea52b6e1b9c

    # Logout (End-Session) vars
    # See https://connect2id.com/products/server/docs/integration/logout-session
    logout:
        resourcePath: /logout-sessions/rest/v1
        accessToken: ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6
        #redirectUri: optional
```


## Development

To build this project install and configure the following dependencies in your
development environment:

1. [Node.js][]: Node is used to run a development web server and build the project.
2. [Yarn][] or [NPM][]: Yarn/NPM is used to manage the Node dependencies.
3. [Bower][]: Bower is used for downloading the latest frontend libraries.

After installing Node, you should be able to run the following command to
install the necessary development tools. You only need to run this command when
dependencies in [package.json](package.json) change.

    $ yarn install && bower install

[Gulp][] is the build system. Install the Gulp command-line tool globally with:

    $ yarn global add gulp-cli

Run the following commands in two separate terminals to create a blissful
development experience where your browser auto-refreshes as soon as files
change on your hard drive.

    $ ./mvnw
    $ gulp

[Bower][] is used to manage the CSS and JavaScript dependencies in this
application. You can upgrade the dependencies by specifying a newer version in
[bower.json](bower.json). You can also run `bower update` and `bower install`
to manage the dependencies. Append the `-h` flag to any command to see how you
can use it. For example, `bower update -h`.


## Implementing the `UserAuthenticator` interface

The `UserAuthenticator` interface facilitates the user authentication, and is
generic enough to support a variety of authentication factors. It has a single
method `authenticate(User user)`. An example LDAP-directory-based
implementation is provided by the `LdapUserAuthenticator` class. You can
load your own implementations from the classpath using the Java `ServiceLoader`
mechanism. To do so edit the `application.yml` configuration file to include
`c2id.authenticatorClass = CustomUserAuthenticator`, where the name of the
class matches the implementing class that will be loaded.

Additionally, you can clone this project and implement the interface like so:

```java
@Primary
@Component
public class CustomUserAuthenticator implements UserAuthenticator {
    public AuthenticationResult authenticate(User user) throws AuthenticationException {
        // use your own backend to authenticate users
        if (user.passwordMatches()) {
            return new AuthenticationResult(user, {"admin": true});
        } else {
            throw new AuthenticationException("Bad credentials.");
        }
    }
}
```
The `@Primary` annotation tells Spring to autowire this bean with priority over
others.


## Building for production

To optimize the c2id-login application for production, run:

    ./mvnw -Pprod clean package

This will concatenate and minify the client CSS and JavaScript files. It will
also modify `index.html` so it references these new files. To ensure everything
worked run:

    java -jar target/*.war

Then navigate to [http://localhost:8080](http://localhost:8080) in your
browser.


## Testing

To launch your application's tests run:

    ./mvnw clean test


### Client tests

Unit tests are run by [Karma][] and written with [Jasmine][]. They're located
in [src/test/javascript/](src/test/javascript/) and can be run with:

    gulp test



## Using Docker to simplify development (optional)

You can fully dockerize your application and all services it depends on. To
achieve this, first build a docker image of your app by running:

    ./mvnw package -Pprod docker:build

Then run:

    docker-compose -f src/main/docker/app.yml up -d



[Node.js]: https://nodejs.org/
[NPM]: https://npmjs.com/
[Yarn]: https://yarnpkg.org/
[Bower]: http://bower.io/
[Gulp]: http://gulpjs.com/
[BrowserSync]: http://www.browsersync.io/
[Karma]: http://karma-runner.github.io/
[Jasmine]: http://jasmine.github.io/2.0/introduction.html
[Protractor]: https://angular.github.io/protractor/
[Leaflet]: http://leafletjs.com/
[DefinitelyTyped]: http://definitelytyped.org/



## Questions or comments?

Email [Connect2id support](https://connect2id.com/contact).


***
Copyright (c) Connect2id Ltd., 2013 - 2021
