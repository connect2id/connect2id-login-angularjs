package com.nimbusds.login.rest;

import com.nimbusds.login.domain.User;
import com.nimbusds.login.rest.errors.AuthenticationException;
import com.nimbusds.login.rest.utils.CookieManager;
import com.nimbusds.login.service.C2idAuthService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import java.io.IOException;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api")
public class C2idAuthResource {

	private final Logger logger = LoggerFactory.getLogger(C2idAuthResource.class);

	private final C2idAuthService authService;

	public C2idAuthResource(C2idAuthService authService) {
		this.authService = authService;
	}

	@GetMapping(path = "/meta", produces = "application/json;charset=UTF-8")
	public String meta() throws IOException {
		return authService.getServerMetadata();
	}

	@PostMapping(path = "/initAuthRequest", produces = "application/json;charset=UTF-8")
	public Map<String, Object> initAuthRequest(@RequestBody Map<String, Object> body,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		String sessionId = CookieManager.getCookie(request, authService.getConfig().getCookieName());
		String queryString = (String) body.get("qs");
		logger.debug("Initial authorization request to Connect2id server: sid={}, qs={}", sessionId, queryString);
		Map<String, Object> authzResponse = authService.initAuthRequest(queryString, sessionId);
		updateSessionIdCookie(authzResponse, request, response);
		return authzResponse;
	}

	@PostMapping(path = "/authenticateSubject", produces = "application/json;charset=UTF-8")
	public Map<String, Object> authenticateSubject(@RequestBody @Valid User user, @RequestParam String authSessionId,
			HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException {
		logger.debug("Making 'user.authGet' request to the LdapAuth service for user '{}'", user.getUsername());
		Map<String, Object> authzResponse = authService.authenticateSubject(user, authSessionId);
		updateSessionIdCookie(authzResponse, request, response);
		return authzResponse;
	}

	@GetMapping(path = "/verifySubject", produces = "application/json;charset=UTF-8")
	public Map<String, Object> verifySubject(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String sessionId = CookieManager.getCookie(request, authService.getConfig().getCookieName());
		logger.debug("Submitting SID for verification (sid={}) to Connect2id server: {}", sessionId);
		Map<String, Object> authzResponse = authService.verifySubject(sessionId);
		return authzResponse;
	}

	@PostMapping(path = "/logoutSession", produces = "application/json;charset=UTF-8")
	public Map<String, Object> logoutSession(@RequestBody Map<String, Object> body,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		String sessionId = CookieManager.getCookie(request, authService.getConfig().getCookieName());
		String queryString = (String) body.get("qs");
		logger.debug("Logout session (sid={}): {}", sessionId);
		Map<String, Object> authzResponse = authService.logoutSession(queryString, sessionId);
		return authzResponse;
	}

	@PutMapping(path = "/logoutSessionConfirm", produces = "application/json;charset=UTF-8")
	public Map<String, Object> logoutSessionConfirm(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String sessionId = CookieManager.getCookie(request, authService.getConfig().getCookieName());
		logger.debug("Logout session (sid={}): {}", sessionId);
		Map<String, Object> authzResponse = authService.logoutSessionConfirm(sessionId);
		if (authzResponse != null && "end".equals(authzResponse.get("type"))) {
			CookieManager.removeCookie(authService.getConfig().getCookieName(), response);
		}
		return authzResponse;
	}

	@PutMapping(path = "/updateAuthRequest/{authSessionId}", produces = "application/json;charset=UTF-8")
	public Map<String, Object> updateAuthRequest(@PathVariable String authSessionId, @RequestBody Map<String, Object> body)
			throws IOException {
		logger.debug("Submitting subject authentication (sid={}) to Connect2id server: {}", authSessionId, body);
		Map<String, Object> authzResponse = authService.updateAuthRequest(authSessionId, body);
		return authzResponse;
	}

	@DeleteMapping(path = "/cancelAuthRequest/{authSessionId}", produces = "application/json;charset=UTF-8")
	public Map<String, Object> cancelAuthRequest(@PathVariable String authSessionId,
			HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.debug("Authorization denied, submitting request to Connect2id server...");
		Map<String, Object> authzResponse = authService.cancelAuthRequest(authSessionId);
		updateSessionIdCookie(authzResponse, request, response);
		return authzResponse;
	}

	@DeleteMapping("/logoutSubject")
	public void logoutSubject(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.debug("Submitting subject logout request to Connect2id server.");
		String sessionId = CookieManager.getCookie(request, authService.getConfig().getCookieName());
		authService.logoutSubject(sessionId);
		CookieManager.removeCookie(authService.getConfig().getCookieName(), response);
	}

	@SuppressWarnings("unchecked")
	private void updateSessionIdCookie(Map<String, Object> authzResponse,
			HttpServletRequest request, HttpServletResponse response) {
		if (authzResponse != null && authzResponse.containsKey("type")) {
			if ("auth".equals(authzResponse.get("type"))) {
				CookieManager.removeCookie(authService.getConfig().getCookieName(), response);
			} else if ("consent".equals(authzResponse.get("type")) && authzResponse.containsKey("sub_session")) {
				Map<String, Object> sub = (Map<String, Object>) authzResponse.get("sub_session");
				String cookieName = authService.getConfig().getCookieName();
				String cookieValue = (String) sub.get("sid");
				int maxLife = (sub.containsKey("max_life") ? (int) sub.get("max_life") : 20160) * 60;
				if (!StringUtils.isBlank(cookieValue)) {
					CookieManager.addCookie(cookieName, cookieValue, request, response, true, maxLife);
				}
			} else if ("response".equals(authzResponse.get("type"))) {
				String cookieName = authService.getConfig().getCookieName();
				String cookieValue = (String) authzResponse.get("sub_sid");
				if (!StringUtils.isBlank(cookieValue)) {
					CookieManager.addCookie(cookieName, cookieValue, request, response, true, 20160 * 60);
				}
			}
		}
	}
}
