(function() {
    'use strict';

    angular
        .module('C2idLoginApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$rootScope', '$scope', '$location', '$cookies', 'Auth'];

    function HomeController ($rootScope, $scope, $location, $cookies, Auth) {
		$scope.noconnection = true;
		$scope.hasQueryString = true;

		if ($location.search().client_id) {
			var qs = $location.absUrl();
			Auth.initAuthRequest({
				qs: qs.substring(qs.indexOf("?") + 1)
			}, function (c2idResponse) {
				$rootScope.$broadcast('event:switch-action', c2idResponse);
			}, function (err) {
				$rootScope.$broadcast('event:auth-error', err.data.message);
			});
		} else {
			$scope.hasQueryString = false;
			if ($scope.DEBUG_INFO_ENABLED) {
				Auth.pingServer(function (data) {
					$scope.noconnection = false;
					$rootScope.metadata = data;
				}, function () {
					$scope.noconnection = true;
				});
			}
			Auth.verifySubject(function (c2idResponse) {
				$rootScope.authenticatedUser = c2idResponse.data;
			}, function (err) {
				$scope.hasQueryString = false;
			});
		}

		$scope.logout = function () {
			Auth.logoutSession({qs: ""}, function (c2idResponse) {
				if (c2idResponse.type && c2idResponse.type === "confirm") {
					$scope.logoutPrompt = true;
					$scope.logoutError = false;
				} else if (c2idResponse.type && c2idResponse.type === "error" || c2idResponse.error) {
					$scope.logoutPrompt = false;
					$scope.logoutError = c2idResponse.error_description || c2idResponse.error;
				}
			}, function (err) {
				$scope.logoutError = err.data.message;
				console.error(err.data.message);
			});
		};

		$scope.logoutConfirm = function () {
			Auth.logoutSessionConfirm({qs: ""}, function (c2idResponse) {
				if (c2idResponse.type && c2idResponse.type === "end") {
					if (c2idResponse.post_logout_redirect_uri) {
						window.location.href = c2idResponse.post_logout_redirect_uri;
					} else {
						$scope.logoutEnd = true;
						$scope.logoutPrompt = false;
						$scope.logoutError = false;
						delete $rootScope.authenticatedUser;
					}
				} else {
					console.log(c2idResponse);
				}
			}, function (err) {
				$scope.logoutError = err.data.message;
				console.error(err.data.message);
			});
		};

		$scope.logoutCancel = function () {
			$scope.logoutPrompt = false;
		};
    }
})();
