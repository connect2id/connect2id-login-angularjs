(function() {
    'use strict';

    angular
        .module('C2idLoginApp', [
            'ngStorage',
            'tmh.dynamicLocale',
            'pascalprecht.translate',
            'ngResource',
            'ngCookies',
            'ngAria',
            'ngRoute',
            'ngCacheBuster',
            'angular-loading-bar'
        ])
		.config(config)
        .run(run);

    config.$inject = ['$routeProvider', '$locationProvider', 'httpRequestInterceptorCacheBusterProvider'];
    run.$inject = ['$rootScope', '$location', 'Base64', 'translationHandler', 'VERSION', 'DEBUG_INFO_ENABLED'];

    function config($routeProvider, $locationProvider, httpRequestInterceptorCacheBusterProvider) {
		//Cache everything except rest api requests
        httpRequestInterceptorCacheBusterProvider.setMatchlist([/.*api.*/], true);
		$locationProvider.html5Mode(true).hashPrefix('!');

		$routeProvider
			.when('/', {
				templateUrl: 'app/home/home.html',
				controller: 'HomeController'
			})
			.when('/login', {
				templateUrl: 'app/login/login.html',
				controller: 'LoginController'
			})
			.when('/consent', {
				templateUrl: 'app/consent/consent.html',
				controller: 'ConsentController'
			})
			.when('/err/:msg?', {
				templateUrl: 'app/error/error.html',
				controller: 'ErrorController'
			})
			.otherwise({
				redirectTo: '/'
			});
    }

    function run($rootScope, $location, Base64, translationHandler, VERSION, DEBUG_INFO_ENABLED) {
		$rootScope.VERSION = VERSION;
		$rootScope.DEBUG_INFO_ENABLED = DEBUG_INFO_ENABLED;
		translationHandler.initialize();

		$rootScope.$on('event:switch-action', function(event, c2idResponse) {
			$rootScope.metadata = angular.copy($location.search()) || {};
			$rootScope.consentedScopes = ['openid'];
			$rootScope.consentedClaims = [];
			$rootScope.metadata.authzResponse = c2idResponse;
			$rootScope.authenticationError = false;
			console.log("Received '" + c2idResponse.type + "' message: ", c2idResponse);

			if (c2idResponse.type === "auth") {
				$location.path('/login').search('authSessionId', c2idResponse.sid);
			} else if (c2idResponse.type === "consent") {
				if (c2idResponse.sub_session) {
					// Set/update subject session cookie
					$rootScope.authenticatedUser = c2idResponse.sub_session.data;
				}
				if (c2idResponse.scope && c2idResponse.scope.consented) {
					$rootScope.consentedScopes = $rootScope.consentedScopes.concat(angular.copy(c2idResponse.scope.consented));
				}
				if (c2idResponse.claims && c2idResponse.claims.consented) {
					$rootScope.consentedClaims = $rootScope.consentedClaims.concat(angular.copy(c2idResponse.claims.consented.voluntary));
					$rootScope.consentedClaims = $rootScope.consentedClaims.concat(c2idResponse.claims.consented.essential);
				}
				$location.path('/consent').search('authSessionId', c2idResponse.sid);
			} else if (c2idResponse.type === "response") {
				// Relays the final OpenID authentication response back to the client
				if (c2idResponse.mode === "query" || c2idResponse.mode === "fragment" || c2idResponse.mode === "query.jwt" || c2idResponse.mode === "fragment.jwt") {
					console.log("Redirection URI: " + c2idResponse.parameters.uri);
					window.location.href = c2idResponse.parameters.uri;
				} else if (c2idResponse.mode === "form_post" || c2idResponse.mode === "form_post.jwt") {
					submitForm(c2idResponse.parameters);
				}
			} else {
				handleAuthError($rootScope, $location, Base64, c2idResponse.error_description);
			}
		});

		$rootScope.$on('event:auth-error', function(event, msg) {
			handleAuthError($rootScope, $location, Base64, msg);
		});
    }

	function submitForm(params) {
		var form1 = document.getElementById('Form1');
		form1.setAttribute("action", params.uri);
		for (var el in params.form) {
			var hidden = document.createElement('input');
			hidden.type = "hidden";
			hidden.name = el;
			hidden.value = params.form[el];
			form1.appendChild(hidden);
		}
		form1.submit();
	}

	function handleAuthError($rootScope, $location, Base64, message) {
		var msg = message || "Authentication failed.";
		console.error(msg);
		$location.path('/err/' + Base64.encode(msg));
		$rootScope.authenticationError = true;
	}
})();
